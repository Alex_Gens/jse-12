package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.SortType;
import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    public void merge(@NotNull final Task entity) throws Exception;

    public void removeWithProject(@NotNull final String currentUserId, @NotNull final String projectId) throws SQLException;

    public void removeAll(@NotNull final String currentUserId) throws SQLException;

    public void removeAllWithProjects(@NotNull final String currentUserId) throws SQLException;

    @Nullable
    public Task findByName(@NotNull final String currentUserId, @NotNull final String name) throws SQLException;

    @NotNull
    public List<Task> findAll(@NotNull final String currentUserId) throws SQLException;

    @NotNull
    public List<String> findAll(@NotNull final String currentUserId, @NotNull final SortType sortType) throws SQLException;

    @NotNull
    public List<String> findAll(@NotNull final String currentUserId,
                                @NotNull final String projectId,
                                @NotNull final SortType sortType) throws SQLException;
    @NotNull
    public List<String> findAllByName(@NotNull final String currentUserId, @NotNull final String part) throws SQLException;

    @NotNull
    public List<String> findAllByInfo(@NotNull final String currentUserId, @NotNull final String part) throws SQLException;

    public boolean contains(@NotNull final String name) throws SQLException;

    public boolean contains(@NotNull final String currentUserId, @NotNull final String name) throws SQLException;

    public boolean isEmpty(@NotNull final String id) throws SQLException;

    public boolean isEmptyRepository(@NotNull final String currentUserId) throws SQLException;

}
