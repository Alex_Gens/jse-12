package ru.kazakov.iteco.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.entity.User;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IUserEndpoint {

    @WebMethod
    public void mergeUser(@Nullable final Session userSession,
                          @Nullable final User entity) throws Exception;

    @WebMethod
    public void persistUser(@Nullable final User entity) throws Exception;

    @WebMethod
    public void removeUser(@Nullable final Session userSession,
                           @Nullable final String id) throws Exception;

    @WebMethod
    public void removeAllUsers(@Nullable final Session userSession) throws Exception;

    @Nullable
    @WebMethod
    public User findOneUser(@Nullable final Session userSession,
                            @Nullable final String id) throws Exception;

    @Nullable
    @WebMethod
    public User findUserByLogin(@Nullable final Session userSession, @Nullable final String login) throws Exception;

    @Nullable
    @WebMethod
    public List<String> findAllUsers(@Nullable final Session userSession) throws Exception;

    @WebMethod
    public boolean containsUser(@Nullable final String login) throws Exception;

    @WebMethod
    public boolean isEmptyUserRepository(@Nullable final Session userSession) throws Exception;

}
