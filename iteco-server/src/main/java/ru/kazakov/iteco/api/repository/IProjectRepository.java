package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.enumeration.SortType;
import java.util.List;

public interface IProjectRepository {

    public void persist(final @NotNull Project entity) throws Exception;

    public void merge(@NotNull final Project entity) throws Exception;

    public void remove(final @NotNull String id) throws Exception;

    public void removeAll() throws Exception;

    public void removeAll(@NotNull final String currentUserId) throws Exception;

    @Nullable
    public Project findOne(final @NotNull String id) throws Exception;

    @Nullable
    public Project findByName(@NotNull final String currentUserId, @NotNull final String name) throws Exception;

    @NotNull
    public List<Project> findAll() throws Exception;

    @NotNull
    public List<Project> findAll(@NotNull final String currentUserId) throws Exception;

    @NotNull
    public List<String> findAll(@NotNull final String currentUserId, @NotNull final SortType sortType) throws Exception;

    @NotNull
    public List<String> findAllByName(@NotNull final String currentUserId, @NotNull final String part) throws Exception;

    @NotNull
    public List<String> findAllByInfo(@NotNull final String currentUserId, @NotNull final String part) throws Exception;

    public boolean contains(@NotNull final String name) throws Exception;

    public boolean contains(@NotNull final String currentUserId, @NotNull final String name) throws Exception;

    public boolean isEmpty(@NotNull final String id) throws Exception;

    public boolean isEmptyRepository(@NotNull final String currentUserId) throws Exception;

}
