package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.kazakov.iteco.entity.Session;
import java.sql.SQLException;

public interface ISessionRepository extends IRepository<Session> {

    public boolean contains(@NotNull final String userId, @NotNull final String id) throws SQLException;

}
