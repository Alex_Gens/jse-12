package ru.kazakov.iteco.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Session;
import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint {

    @WebMethod
    public void saveDomainBin(@Nullable final Session userSession,
                              @Nullable final String directory,
                              @Nullable final String fileName) throws java.lang.Exception;

    @WebMethod
    public void saveDomainJaxbXml(@Nullable final Session userSession,
                                  @Nullable final String directory,
                                  @Nullable final String fileName) throws java.lang.Exception;

    @WebMethod
    public void saveDomainJaxbJson(@Nullable final Session userSession,
                                   @Nullable final String directory,
                                   @Nullable final String fileName) throws java.lang.Exception;

    @WebMethod
    public void saveDomainFasterXml(@Nullable final Session userSession,
                                    @Nullable final String directory,
                                    @Nullable final String fileName) throws java.lang.Exception;

    @WebMethod
    public void saveDomainFasterJson(@Nullable final Session userSession,
                                     @Nullable final String directory,
                                     @Nullable final String fileName) throws java.lang.Exception;

    @WebMethod
    public void loadDomainBin(@Nullable final Session userSession,
                              @Nullable final String directory,
                              @Nullable final String fileName) throws java.lang.Exception;

    @WebMethod
    public void loadDomainJaxbXml(@Nullable final Session userSession,
                                  @Nullable final String directory,
                                  @Nullable final String fileName) throws java.lang.Exception;

    @WebMethod
    public void loadDomainJaxbJson(@Nullable final Session userSession,
                                   @Nullable final String directory,
                                   @Nullable final String fileName) throws java.lang.Exception;

    @WebMethod
    public void loadDomainFasterXml(@Nullable final Session userSession,
                                    @Nullable final String directory,
                                    @Nullable final String fileName) throws java.lang.Exception;

    @WebMethod
    public void loadDomainFasterJson(@Nullable final Session userSession,
                                     @Nullable final String directory,
                                     @Nullable final String fileName) throws java.lang.Exception;

    @WebMethod
    public boolean isDomainDirectory(@Nullable final Session userSession,
                                     @Nullable final String directory) throws java.lang.Exception;

    @WebMethod
    public boolean existDomain(@Nullable final Session userSession,
                               @Nullable final String directory,
                               @Nullable final String fileName) throws java.lang.Exception;

}
