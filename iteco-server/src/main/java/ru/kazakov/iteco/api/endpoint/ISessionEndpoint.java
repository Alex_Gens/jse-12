package ru.kazakov.iteco.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.Session;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ISessionEndpoint {

    @WebMethod
    public Session getInstanceSession(@Nullable final String login,
                                      @Nullable final String password) throws Exception;

    @WebMethod
    public void persistSession(@Nullable Session entity) throws Exception;

    @WebMethod
    public void removeSession(@Nullable String id) throws Exception;

    @WebMethod
    public void removeAllSessions() throws Exception;

    @Nullable
    @WebMethod
    public Session findOneSession(@Nullable String id) throws Exception;

    @Nullable
    @WebMethod
    public List<Session> findAllSessions() throws Exception;

    @WebMethod
    public boolean containsSession(@Nullable final String userId,
                                   @Nullable final String id) throws Exception;

}
