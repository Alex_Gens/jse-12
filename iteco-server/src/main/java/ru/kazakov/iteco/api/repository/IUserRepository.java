package ru.kazakov.iteco.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.entity.User;
import java.sql.SQLException;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    public void merge(@NotNull final User entity) throws Exception;

    @Nullable
    public User findByLogin(@NotNull final String login) throws Exception;

    @NotNull
    public List<String> findAllUsers() throws SQLException;

    public boolean contains(@NotNull final String login) throws Exception;

}
