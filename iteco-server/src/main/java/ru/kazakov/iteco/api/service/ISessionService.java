package ru.kazakov.iteco.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.entity.Session;

public interface ISessionService extends IService<Session> {

    @Nullable
    public Session getInstance(@Nullable final String login,
                               @Nullable final String password,
                               @NotNull final ServiceLocator serviceLocator) throws Exception;

    public boolean contains(@Nullable final String userId, @Nullable final String id) throws Exception;

}
