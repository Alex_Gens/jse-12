package ru.kazakov.iteco.util;

import org.jetbrains.annotations.NotNull;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;


public final class SqlConnector {

    public Connection getConnection() throws IOException, SQLException {
        @NotNull final Properties databaseProperty = new Properties();
        try(InputStream dataBaseStream = SqlConnector.class.getClassLoader()
                .getResourceAsStream("database.properties")) {
            databaseProperty.load(dataBaseStream);
            Class.forName(databaseProperty.getProperty("driver"));
        } catch (ClassNotFoundException classNotFoundException) {
            classNotFoundException.printStackTrace();
        }
        @NotNull final String url = databaseProperty.getProperty("url");
        @NotNull final String user = databaseProperty.getProperty("user");
        @NotNull final String password = databaseProperty.getProperty("password");
        @NotNull final Connection connection = DriverManager.getConnection(url, user, password);
        return connection;
    }

}
