package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.repository.IUserRepository;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.repository.UserRepository;
import ru.kazakov.iteco.util.SqlConnector;
import java.sql.Connection;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    @Override
    public void persist(@Nullable final User entity) throws Exception {
        if (entity == null) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IUserRepository repository = new UserRepository(connection);
        repository.persist(entity);
        connection.close();
    }

    @Override
    public void merge(@Nullable final User entity) throws Exception {
        if (entity == null) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IUserRepository repository = new UserRepository(connection);
        repository.merge(entity);
        connection.close();
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IUserRepository repository = new UserRepository(connection);
        repository.remove(id);
        connection.close();
    }

    @Override
    public void removeAll() throws Exception {
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IUserRepository repository = new UserRepository(connection);
        repository.removeAll();
        connection.close();
    }

    @Nullable
    @Override
    public User findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IUserRepository repository = new UserRepository(connection);
        @Nullable final User user = repository.findOne(id);
        connection.close();
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IUserRepository repository = new UserRepository(connection);
        @Nullable final User user = repository.findByLogin(login);
        connection.close();
        return user;
    }

    @NotNull
    @Override
    public List<User> findAll() throws Exception {
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IUserRepository repository = new UserRepository(connection);
        @Nullable final List<User> users = repository.findAll();
        connection.close();
        return users;
    }

    @NotNull
    @Override
    public List<String> findAllUsers() throws Exception {
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IUserRepository repository = new UserRepository(connection);
        @Nullable final List<String> usersNames = repository.findAllUsers();
        connection.close();
        return usersNames;
    }

    @Override
    public boolean contains(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IUserRepository repository = new UserRepository(connection);
        @Nullable final boolean contains = repository.contains(login);
        connection.close();
        return contains;
    }

}
