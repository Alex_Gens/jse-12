package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.repository.IProjectRepository;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.enumeration.SortType;
import ru.kazakov.iteco.repository.ProjectRepository;
import ru.kazakov.iteco.util.SqlConnector;
import java.sql.Connection;
import java.util.List;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    @Override
    public void persist(@Nullable final Project entity) throws Exception {
        if (entity == null) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        repository.persist(entity);
        connection.close();
    }

    @Override
    public void merge(@Nullable final Project entity) throws Exception {
        if (entity == null) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        repository.merge(entity);
        connection.close();
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        repository.remove(id);
        connection.close();
    }

    @Override
    public void removeAll() throws Exception {
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        repository.removeAll();
        connection.close();
    }

    @Override
    public void removeAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        repository.removeAll(currentUserId);
        connection.close();
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @Nullable final Project project = repository.findOne(id);
        connection.close();
        return project;
    }

    @Nullable
    @Override
    public Project findByName(@Nullable final String currentUserId, @Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @Nullable final Project project = repository.findByName(currentUserId, name);
        connection.close();
        return project;
    }

    @NotNull
    @Override
    public List<Project> findAll() throws Exception {
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @Nullable final List<Project> projects = repository.findAll();
        connection.close();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @NotNull final List<Project> projects = repository.findAll(currentUserId);
        connection.close();
        return projects;
    }

    @Nullable
    @Override
    public List<String> findAll(@Nullable final String currentUserId, @Nullable final SortType sortType) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        if (sortType == null) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @NotNull final List<String> projectsNames = repository.findAll(currentUserId, sortType);
        connection.close();
        return projectsNames;
    }

    @Nullable
    @Override
    public List<String> findAllByName(@Nullable final String currentUserId, @Nullable final String part) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @NotNull final List<String> projectsNames = repository.findAllByName(currentUserId, part);
        connection.close();
        return projectsNames;
    }

    @Nullable
    @Override
    public List<String> findAllByInfo(@Nullable final String currentUserId, @Nullable final String part) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        @NotNull final List<String> projectsNames = repository.findAllByInfo(currentUserId, part);
        connection.close();
        return projectsNames;
    }

    @Override
    public boolean contains(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        final boolean contains = repository.contains(name);
        connection.close();
        return contains;
    }

    @Override
    public boolean contains(@Nullable final String currentUserId, @Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        final boolean contains = repository.contains(currentUserId, name);
        connection.close();
        return contains;
    }

    @Override
    public boolean isEmpty(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        final boolean isEmpty = repository.isEmpty(id);
        connection.close();
        return isEmpty;
    }

    @Override
    public boolean isEmptyRepository(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final IProjectRepository repository = new ProjectRepository(connection);
        final boolean isEmpty = repository.isEmptyRepository(currentUserId);
        connection.close();
        return isEmpty;
    }

}
