package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.repository.ISessionRepository;
import ru.kazakov.iteco.api.service.ISessionService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.repository.SessionRepository;
import ru.kazakov.iteco.util.Password;
import ru.kazakov.iteco.util.SignatureUtil;
import ru.kazakov.iteco.util.SqlConnector;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @Override
    public void persist(@Nullable final Session entity) throws Exception {
        if (entity == null) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ISessionRepository repository = new SessionRepository(connection);
        repository.persist(entity);
        connection.close();
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ISessionRepository repository = new SessionRepository(connection);
        repository.remove(id);
        connection.close();
    }

    @Override
    public void removeAll() throws Exception {
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ISessionRepository repository = new SessionRepository(connection);
        repository.removeAll();
        connection.close();
    }

    @Nullable
    @Override
    public Session findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ISessionRepository repository = new SessionRepository(connection);
        @Nullable final Session session = repository.findOne(id);
        connection.close();
        return session;
    }

    @NotNull
    @Override
    public List<Session> findAll() throws Exception {
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ISessionRepository repository = new SessionRepository(connection);
        @Nullable final List<Session> sessions = repository.findAll();
        connection.close();
        return sessions;
    }

    @Nullable
    @Override
    public Session getInstance(@Nullable final String login,
                               @Nullable final String password,
                               @NotNull final ServiceLocator serviceLocator) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception();
        if (password == null || password.isEmpty()) throw new Exception();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new Exception();
        if (user.getPassword() == null || user.getPassword().isEmpty()) throw new Exception();
        @NotNull final String enteredPassword = Password.getHashedPassword(password);
        if (user.getPassword().equals(enteredPassword)) {
            user.setDateStart(new Date(System.currentTimeMillis()));
            userService.merge(user);
            @NotNull final Session userSession = new Session();
            userSession.setRoleType(user.getRoleType());
            userSession.setUserId(user.getId());
            @Nullable final String signature = SignatureUtil.getSignature(userSession);
            if (signature == null || signature.isEmpty()) throw new Exception();
            userSession.setSignature(signature);
            return userSession;
        }
        return null;
    }

    @Override
    public boolean contains(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (id == null || id.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ISessionRepository repository = new SessionRepository(connection);
        @Nullable final boolean contains = repository.contains(userId, id);
        connection.close();
        return contains;
    }

}
