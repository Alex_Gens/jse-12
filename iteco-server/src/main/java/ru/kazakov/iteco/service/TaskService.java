package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.repository.ITaskRepository;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.SortType;
import ru.kazakov.iteco.repository.TaskRepository;
import ru.kazakov.iteco.util.SqlConnector;
import java.sql.Connection;
import java.util.List;

public class TaskService extends AbstractService<Task> implements ITaskService {

    @Override
    public void persist(@Nullable final Task entity) throws Exception {
        if (entity == null) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        repository.persist(entity);
        connection.close();
    }

    @Override
    public void merge(@Nullable final Task entity) throws Exception {
        if (entity == null) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        repository.merge(entity);
        connection.close();
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        repository.remove(id);
        connection.close();
    }

    @Override
    public void removeWithProject(@Nullable final String currentUserId, @Nullable final String projectId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        if (projectId == null || projectId.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        repository.removeWithProject(currentUserId, projectId);
        connection.close();
    }

    @Override
    public void removeAll() throws Exception {
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        repository.removeAll();
        connection.close();
    }

    @Override
    public void removeAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        repository.removeAll(currentUserId);
        connection.close();
    }

    @Override
    public void removeAllWithProjects(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        repository.removeAllWithProjects(currentUserId);
        connection.close();
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @Nullable final Task task = repository.findOne(id);
        connection.close();
        return task;
    }

    @Nullable
    @Override
    public Task findByName(@Nullable final String currentUserId, @Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @Nullable final Task task = repository.findByName(currentUserId, name);
        connection.close();
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAll() throws Exception {
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @Nullable final List<Task> tasks = repository.findAll();
        connection.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @Nullable final List<Task> tasks = repository.findAll(currentUserId);
        connection.close();
        return tasks;
    }

    @Nullable
    @Override
    public List<String> findAll(@Nullable final String currentUserId, @Nullable final SortType sortType) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        if (sortType == null) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @Nullable final List<String> tasksNames = repository.findAll(currentUserId, sortType);
        connection.close();
        return tasksNames;
    }

    @Nullable
    @Override
    public List<String> findAll(@Nullable final String currentUserId,
                                @Nullable final String projectId,
                                @Nullable final SortType sortType) throws Exception {
        if (projectId == null || projectId.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        if (sortType == null) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @Nullable final List<String> tasksNames = repository.findAll(currentUserId, projectId, sortType);
        connection.close();
        return tasksNames;
    }

    @Nullable
    @Override
    public List<String> findAllByName(@Nullable final String currentUserId, @Nullable final String part) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @Nullable final List<String> tasksNames = repository.findAllByName(currentUserId, part);
        connection.close();
        return tasksNames;
    }

    @Nullable
    @Override
    public List<String> findAllByInfo(@Nullable final String currentUserId, @Nullable final String part) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @Nullable final List<String> tasksNames = repository.findAllByInfo(currentUserId, part);
        connection.close();
        return tasksNames;
    }

    @Override
    public boolean contains(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @Nullable final boolean contains = repository.contains(name);
        connection.close();
        return contains;
    }

    @Override
    public boolean contains(@Nullable final String currentUserId, @Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @Nullable final boolean contains = repository.contains(currentUserId, name);
        connection.close();
        return contains;
    }

    @Override
    public boolean isEmpty(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @Nullable final boolean isEmpty = repository.isEmpty(id);
        connection.close();
        return isEmpty;
    }

    @Override
    public boolean isEmptyRepository(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @Nullable final Connection connection = new SqlConnector().getConnection();
        if (connection == null) throw new Exception();
        @NotNull final ITaskRepository repository = new TaskRepository(connection);
        @Nullable final boolean isEmpty = repository.isEmptyRepository(currentUserId);
        connection.close();
        return isEmpty;
    }

}
