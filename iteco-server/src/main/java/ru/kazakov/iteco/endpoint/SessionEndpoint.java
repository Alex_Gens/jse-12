package ru.kazakov.iteco.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.endpoint.ISessionEndpoint;
import ru.kazakov.iteco.api.service.ISessionService;
import ru.kazakov.iteco.entity.Session;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kazakov.iteco.api.endpoint.ISessionEndpoint")
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @NotNull
    private final ISessionService sessionService;

    public SessionEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.sessionService = serviceLocator.getSessionService();
    }

    @Nullable
    @WebMethod
    public Session getInstanceSession(@Nullable final String login,
                                      @Nullable final String password) throws Exception {
        return sessionService.getInstance(login, password, serviceLocator);
    }

    @WebMethod
    public void persistSession(@Nullable final Session entity) throws Exception {sessionService.persist(entity);}

    @WebMethod
    public void removeSession(@Nullable final String id) throws Exception {sessionService.remove(id);}

    @WebMethod
    public void removeAllSessions() throws Exception {sessionService.removeAll();}

    @Nullable
    @WebMethod
    public Session findOneSession(@Nullable final String id) throws Exception {return sessionService.findOne(id);}

    @Nullable
    @WebMethod
    public List<Session> findAllSessions() throws Exception {return sessionService.findAll();}

    @WebMethod
    public boolean containsSession(@Nullable final String userId,
                                   @Nullable final String id) throws Exception {
        return sessionService.contains(userId, id);
    }

}
