package ru.kazakov.iteco.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.endpoint.IDomainEndpoint;
import ru.kazakov.iteco.api.service.IDomainService;
import ru.kazakov.iteco.entity.Session;
import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.kazakov.iteco.api.endpoint.IDomainEndpoint")
public class DomainEndpoint extends AbstractEndpoint implements IDomainEndpoint {

    @NotNull
    private final IDomainService domainService;

    public DomainEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.domainService = serviceLocator.getDomainService();
    }

    @Override
    @WebMethod
    public void saveDomainBin(@Nullable final Session userSession,
                              @Nullable final String directory,
                              @Nullable final String fileName) throws java.lang.Exception {
        validate(userSession);
        domainService.saveBin(directory, fileName);
    }

    @Override
    @WebMethod
    public void saveDomainJaxbXml(@Nullable final Session userSession,
                                  @Nullable final String directory,
                                  @Nullable final String fileName) throws java.lang.Exception {
        validate(userSession);
        domainService.saveJaxbXml(directory, fileName);
    }

    @Override
    @WebMethod
    public void saveDomainJaxbJson(@Nullable final Session userSession,
                                   @Nullable final String directory,
                                   @Nullable final String fileName) throws java.lang.Exception {
        validate(userSession);
        domainService.saveJaxbJson(directory, fileName);
    }

    @Override
    @WebMethod
    public void saveDomainFasterXml(@Nullable final Session userSession,
                                    @Nullable final String directory,
                                    @Nullable final String fileName) throws java.lang.Exception {
        validate(userSession);
        domainService.saveFasterXml(directory, fileName);
    }

    @Override
    @WebMethod
    public void saveDomainFasterJson(@Nullable final Session userSession,
                                     @Nullable final String directory,
                                     @Nullable final String fileName) throws java.lang.Exception {
        validate(userSession);
        domainService.saveFasterJson(directory, fileName);
    }

    @Override
    @WebMethod
    public void loadDomainBin(@Nullable final Session userSession,
                              @Nullable final String directory,
                              @Nullable final String fileName) throws java.lang.Exception {
        validate(userSession);
        domainService.loadBin(userSession.getId(), directory, fileName);
    }

    @Override
    @WebMethod
    public void loadDomainJaxbXml(@Nullable final Session userSession,
                                  @Nullable final String directory,
                                  @Nullable final String fileName) throws java.lang.Exception {
        validate(userSession);
        domainService.loadJaxbXml(userSession.getId(), directory, fileName);
    }

    @Override
    @WebMethod
    public void loadDomainJaxbJson(@Nullable final Session userSession,
                                   @Nullable final String directory,
                                   @Nullable final String fileName) throws java.lang.Exception {
        validate(userSession);
        domainService.loadJaxbJson(userSession.getId(), directory, fileName);
    }

    @Override
    @WebMethod
    public void loadDomainFasterXml(@Nullable final Session userSession,
                                    @Nullable final String directory,
                                    @Nullable final String fileName) throws java.lang.Exception {
        validate(userSession);
        domainService.loadFasterXml(userSession.getId(), directory, fileName);
    }

    @Override
    @WebMethod
    public void loadDomainFasterJson(@Nullable final Session userSession,
                                     @Nullable final String directory,
                                     @Nullable final String fileName) throws java.lang.Exception {
        validate(userSession);
        domainService.loadFasterJson(userSession.getId(), directory, fileName);
    }

    @Override
    @WebMethod
    public boolean isDomainDirectory(@Nullable final Session userSession,
                                     @Nullable final String directory) throws java.lang.Exception {
        validate(userSession);
        return domainService.isDirectory(directory);
    }

    @Override
    @WebMethod
    public boolean existDomain(@Nullable final Session userSession,
                               @Nullable final String directory,
                               @Nullable final String fileName) throws java.lang.Exception {
        validate(userSession);
        return domainService.exist(directory, fileName);
    }

}
