package ru.kazakov.iteco.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.endpoint.IProjectEndpoint;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.enumeration.SortType;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "ru.kazakov.iteco.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    private final IProjectService projectService;

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.projectService = serviceLocator.getProjectService();
    }

    @Override
    @WebMethod
    public void mergeProject(@Nullable final Session userSession,
                             @Nullable final Project entity) throws Exception {
        validate(userSession);
        projectService.merge(entity);
    }

    @Override
    @WebMethod
    public void persistProject(@Nullable final Session userSession,
                               @Nullable final Project entity) throws Exception {
        validate(userSession);
        projectService.persist(entity);
    }

    @Override
    @WebMethod
    public void removeProjectById(@Nullable final Session userSession,
                                  @Nullable final String id) throws Exception {
        validate(userSession);
        projectService.remove(id);
    }

    @Override
    @WebMethod
    public void removeAllProjects(@Nullable final Session userSession) throws Exception {
        validate(userSession);
        projectService.removeAll();
    }

    @Override
    @WebMethod
    public void removeAllProjectsByCurrentId(@Nullable final Session userSession) throws Exception {
        validate(userSession);
        projectService.removeAll(userSession.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public Project findByProjectNameCurrentId(@Nullable final Session userSession,
                                              @Nullable final String name) throws Exception {
        validate(userSession);
        return projectService.findByName(userSession.getUserId(), name);
    }

    @Nullable
    @Override
    @WebMethod
    public Project findOneProject(@Nullable final Session userSession,
                                  @Nullable final String id) throws Exception {
        validate(userSession);
        return projectService.findOne(id);
    }

    @Nullable
    @Override
    @WebMethod
    public List<Project> findAllProjects(@Nullable final Session userSession) throws Exception {
        validate(userSession);
        return projectService.findAll();
    }

    @Nullable
    @Override
    @WebMethod
    public List<Project> findAllProjectsByCurrentId(@Nullable final Session userSession) throws Exception {
        validate(userSession);
        return projectService.findAll(userSession.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public List<String> findAllSortedProjectsByCurrentId(@Nullable final Session userSession,
                                                         @Nullable final SortType sortType) throws Exception {
        validate(userSession);
        return projectService.findAll(userSession.getUserId(), sortType);
    }

    @Nullable
    @Override
    @WebMethod
    public List<String> findAllProjectsByNameCurrentId(@Nullable final Session userSession,
                                                       @Nullable final String part) throws Exception {
        validate(userSession);
        return projectService.findAllByName(userSession.getUserId(), part);
    }

    @Nullable
    @Override
    @WebMethod
    public List<String> findAllProjectsByInfoCurrentId(@Nullable final Session userSession,
                                                       @Nullable final String part) throws Exception {
        validate(userSession);
        return projectService.findAllByInfo(userSession.getUserId(), part);
    }

    @Override
    @WebMethod
    public boolean containsProject(@Nullable final String name, @Nullable final
    Session userSession) throws Exception {
        validate(userSession);
        return projectService.contains(name);
    }

    @Override
    @WebMethod
    public boolean containsProjectByCurrentId(@Nullable final Session userSession,
                                              @Nullable final String name) throws Exception {
        validate(userSession);
        return projectService.contains(userSession.getUserId(), name);
    }

    @Override
    @WebMethod
    public boolean isEmptyProject(@Nullable final Session userSession,
                                  @Nullable final String id) throws Exception {
        validate(userSession);
        return projectService.isEmpty(id);
    }

    @Override
    @WebMethod
    public boolean isEmptyProjectRepositoryByCurrentId(@Nullable final Session userSession) throws Exception {
        validate(userSession);
        return projectService.isEmptyRepository(userSession.getUserId());
    }

}
