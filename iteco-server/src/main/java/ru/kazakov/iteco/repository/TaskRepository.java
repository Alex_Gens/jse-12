package ru.kazakov.iteco.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.repository.ITaskRepository;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.SortType;
import ru.kazakov.iteco.enumeration.Status;
import ru.kazakov.iteco.util.DateUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import static ru.kazakov.iteco.constant.Constant.*;
import static ru.kazakov.iteco.util.DateUtil.*;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @NotNull
    private final Connection databaseConnection;

    public TaskRepository(@NotNull final Connection databaseConnection) {this.databaseConnection = databaseConnection;}

    @Override
    public void persist(@NotNull final Task entity) throws Exception {
        @NotNull final String query = "insert into task_manager.task values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = getStatement(entity, query);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void merge(@NotNull final Task entity) throws Exception {
        @NotNull final String queryExist = String.format("select exists (select %s from task_manager.task where %s = ? )",
                ID, ID);
        @NotNull final PreparedStatement statementExist = databaseConnection.prepareStatement(queryExist);
        statementExist.setString(1, entity.getId());
        @NotNull final ResultSet resultSet = statementExist.executeQuery();
        resultSet.next();
        final boolean exist = resultSet.getBoolean(1);
        resultSet.close();
        statementExist.close();
        if (!exist) {
            persist(entity);
            return;
        }
        @NotNull String query = "update task_manager.task set ";
        if (entity.getDateStart() != null) query = query + DATE_START + " = ?, ";
        if (entity.getDateFinish() != null) query = query + DATE_FINISH + " = ?, ";
        if (entity.getStatus() != null) query = query + STATUS + " = ?, ";
        if (entity.getInformation() != null && !entity.getInformation().isEmpty()) query = query + INFORMATION + " = ?";
        if (entity.getProjectId() != null && !entity.getProjectId().isEmpty()) query = query + PROJECT_ID + " = ?";
        if (query.endsWith(", ")) query = query.substring(0, query.length() - 2);
        query = query + " where " + ID + " = ? " ;
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        int counter = 1;
        if (entity.getDateStart() != null) {statement.setDate(counter, DateUtil.getSqlDate(entity.getDateStart())); counter++;}
        if (entity.getDateFinish() != null) {statement.setDate(counter, DateUtil.getSqlDate(entity.getDateFinish())); counter++;}
        if (entity.getStatus() != null) {statement.setString(counter, String.valueOf(entity.getStatus())); counter++;}
        if (entity.getInformation() != null && !entity.getInformation().isEmpty()) {statement.setString(counter, entity.getInformation()); counter++;}
        if (entity.getProjectId() != null && !entity.getProjectId().isEmpty()) {statement.setString(counter, entity.getProjectId()); counter++;}
        statement.setString(counter, entity.getId());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void remove(@NotNull final String id) throws Exception {
        @NotNull final String query = String.format("delete from task_manager.task where %s = ?", ID);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, id);
        statement.execute();
        statement.close();
    }

    @Override
    public void removeWithProject(@NotNull final String currentUserId, @NotNull final String projectId) throws SQLException {
        @NotNull final String query = String.format("delete from task_manager.task where %s = ? and %s = ?", USER_ID, PROJECT_ID);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, currentUserId);
        statement.setString(2, projectId);
        statement.execute();
        statement.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String query = "delete from task_manager.task";
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.execute();
        statement.close();
    }

    @Override
    public void removeAll(@NotNull final String currentUserId) throws SQLException {
        @NotNull final String query = String.format("delete from task_manager.task where %s = ?", USER_ID);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, currentUserId);
        statement.execute();
        statement.close();
    }

    @Override
    public void removeAllWithProjects(@NotNull final String currentUserId) throws SQLException {
        @NotNull final String query = String.format("delete from task_manager.task where %s = ? and %s is not null", USER_ID, PROJECT_ID);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, currentUserId);
        statement.execute();
        statement.close();
    }

    @Nullable
    @Override
    public Task findOne(@NotNull final String id) throws Exception {
        @NotNull final String query = String.format("select * from task_manager.task where %s = ?", ID);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        @Nullable final Task task = fetch(resultSet);
        resultSet.close();
        statement.close();
        return task;
    }

    @Nullable
    @Override
    public Task findByName(@NotNull final String currentUserId, @NotNull final String name) throws SQLException {
        @NotNull final String query = String.format("select * from task_manager.task where %s = ? and %s = ? ", USER_ID, NAME);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, currentUserId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        @Nullable final Task task = fetch(resultSet);
        resultSet.close();
        statement.close();
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAll() throws Exception {
        @NotNull final String query = "select * from task_manager.task";
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) {
            tasks.add(fetch(resultSet));
        }
        resultSet.close();
        statement.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String currentUserId) throws SQLException {
        @NotNull final String query = String.format("select * from task_manager.task where %s = ? ", USER_ID);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        statement.setString(1, currentUserId);
        @NotNull final List<Task> tasks = new ArrayList<>();
        while (resultSet.next()) {
            tasks.add(fetch(resultSet));
        }
        resultSet.close();
        statement.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<String> findAll(@NotNull final String currentUserId, @NotNull final SortType sortType) throws SQLException {
        @NotNull final String orderBy;
        switch (sortType) {
            case START   : orderBy = DATE_START; break;
            case FINISH  : orderBy = DATE_FINISH; break;
            case STATUS  : orderBy = String.format("field(status, '%s','%s','%s')",
                                     String.valueOf(Status.PLANNED),
                                     String.valueOf(Status.IN_PROGRESS),
                                     String.valueOf(Status.READY)); break;
            default      : orderBy = DATE_CREATE;
        }
        @NotNull final String query = String.format("select %s from task_manager.task where %s = ? order by %s",
                NAME, USER_ID, orderBy);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, currentUserId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<String> names = new ArrayList<>();
        while (resultSet.next()) {
            names.add(resultSet.getString(1));
        }
        resultSet.close();
        statement.close();
        return names;
    }

    @NotNull
    @Override
    public List<String> findAll(@NotNull final String currentUserId,
                                @NotNull final String projectId,
                                @NotNull final SortType sortType) throws SQLException {
        @NotNull final String orderBy;
        switch (sortType) {
            case START   : orderBy = DATE_START; break;
            case FINISH  : orderBy = DATE_FINISH; break;
            case STATUS  : orderBy = String.format("field(status, '%s','%s','%s')",
                                     String.valueOf(Status.PLANNED),
                                     String.valueOf(Status.IN_PROGRESS),
                                     String.valueOf(Status.READY)); break;
            default      : orderBy = DATE_CREATE;
        }
        @NotNull final String query = String.format("select %s from task_manager.task where %s = ? and %s = ? order by %s",
                NAME, USER_ID, PROJECT_ID, orderBy);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, currentUserId);
        statement.setString(2, projectId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<String> names = new ArrayList<>();
        while (resultSet.next()) {
            names.add(resultSet.getString(1));
        }
        resultSet.close();
        statement.close();
        return names;
    }

    @NotNull
    @Override
    public List<String> findAllByName(@NotNull final String currentUserId, @NotNull final String part) throws SQLException {
        @NotNull final String query = String.format("select %s from task_manager.task where %s = ? and %s like ?" ,
                NAME, USER_ID, NAME);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, currentUserId);
        statement.setString(2, "%" + part + "%");
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<String> names = new ArrayList<>();
        while (resultSet.next()) {
            names.add(resultSet.getString(1));
        }
        resultSet.close();
        statement.close();
        return names;
    }

    @NotNull
    @Override
    public List<String> findAllByInfo(@NotNull final String currentUserId, @NotNull final String part) throws SQLException {
        @NotNull final String query = String.format("select %s from task_manager.task where %s = ? and %s like ?",
                NAME, USER_ID, INFORMATION);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, currentUserId);
        statement.setString(2, "%" + part + "%");
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<String> names = new ArrayList<>();
        while (resultSet.next()) {
            names.add(resultSet.getString(1));
        }
        resultSet.close();
        statement.close();
        return names;
    }

    @Override
    public boolean contains(@NotNull final String name) throws SQLException {
        @NotNull final String query = String.format("select exists (select %s from task_manager.task where %s = ? )",
                ID, NAME);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        final boolean result = resultSet.getBoolean(1);
        resultSet.close();
        statement.close();
        return result;
    }

    @Override
    public boolean contains(@NotNull final String currentUserId, @NotNull final String name) throws SQLException {
        @NotNull final String query = String.format("select exists (select %s from task_manager.task where %s = ? and %s = ? )",
                ID, USER_ID, NAME);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, currentUserId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        final boolean result = resultSet.getBoolean(1);
        resultSet.close();
        statement.close();
        return result;
    }

    @Override
    public boolean isEmpty(@NotNull final String id) throws SQLException {
        @NotNull final String query = String.format("select isnull(%s) from task_manager.task where %s = ?", INFORMATION, ID);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        final boolean result = resultSet.getBoolean(1);
        resultSet.close();
        statement.close();
        return result;
    }

    @Override
    public boolean isEmptyRepository(@NotNull final String currentUserId) throws SQLException {
        @NotNull final String query = String.format("select not exists (select * from task_manager.task where %s = ? )", USER_ID);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, currentUserId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        final boolean result = resultSet.getBoolean(1);
        resultSet.close();
        statement.close();
        return result;
    }

    @Nullable
    private Task fetch(@Nullable final ResultSet resultSet) throws SQLException {
        if (resultSet == null) return null;
        @NotNull final Task task = new Task();
        task.setId(resultSet.getString(ID));
        task.setUserId(resultSet.getString(USER_ID));
        task.setProjectId(resultSet.getString(PROJECT_ID));
        task.setName(resultSet.getString(NAME));
        task.setDateCreate(getDateFromSql(resultSet.getDate(DATE_CREATE)));
        task.setDateStart(getDateFromSql(resultSet.getDate(DATE_START)));
        task.setDateFinish(getDateFromSql(resultSet.getDate(DATE_FINISH)));
        task.setStatus(Status.valueOf(resultSet.getString(STATUS)));
        task.setInformation(resultSet.getString(INFORMATION));
        return task;
    }

    @NotNull
    private PreparedStatement getStatement(@NotNull final Task entity, @NotNull final String query) throws SQLException {
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getUserId());
        statement.setString(3, entity.getProjectId());
        statement.setString(4, entity.getName());
        statement.setDate(5, getSqlDate(entity.getDateCreate()));
        statement.setDate(6, getSqlDate(entity.getDateStart()));
        statement.setDate(7, getSqlDate(entity.getDateFinish()));
        statement.setString(8, String.valueOf(entity.getStatus()));
        statement.setString(9, entity.getInformation());
        return statement;
    }

}
