package ru.kazakov.iteco.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.repository.ISessionRepository;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.enumeration.RoleType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import static ru.kazakov.iteco.constant.Constant.*;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    @NotNull
    private final Connection databaseConnection;

    public SessionRepository(@NotNull final Connection databaseConnection) {this.databaseConnection = databaseConnection;}

    @Override
    public void persist(@NotNull final Session entity) throws Exception {
        @NotNull final String query = "insert into task_manager.session values (?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = getStatement(entity, query);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void remove(@NotNull final String id) throws Exception {
        @NotNull final String query = String.format("delete from task_manager.session where %s = ?", ID);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, id);
        statement.execute();
        statement.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String query = "delete from task_manager.session";
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.execute();
        statement.close();
    }

    @Nullable
    @Override
    public Session findOne(@NotNull final String id) throws Exception {
        @NotNull final String query = String.format("select * from task_manager.session where %s = ?", ID);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        @Nullable final Session session = fetch(resultSet);
        resultSet.close();
        statement.close();
        return session;
    }

    @NotNull
    @Override
    public List<Session> findAll() throws Exception {
        @NotNull final String query = "select * from task_manager.session";
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Session> sessions = new ArrayList<>();
        while (resultSet.next()) {
            sessions.add(fetch(resultSet));
        }
        resultSet.close();
        statement.close();
        return sessions;
    }

    @Override
    public boolean contains(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final String query = String.format("select exists (select %s from task_manager.session where %s = ? and %s = ? )",
                ID, USER_ID, ID);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        final boolean result = resultSet.getBoolean(1);
        resultSet.close();
        statement.close();
        return result;
    }

    @Nullable
    private Session fetch(@Nullable final ResultSet resultSet) throws SQLException {
        if (resultSet == null) return null;
        @NotNull final Session session = new Session();
        session.setId(resultSet.getString(ID));
        session.setUserId(resultSet.getString(USER_ID));
        session.setTimestamp(resultSet.getLong(TIMESTAMP));
        session.setRoleType(RoleType.valueOf(resultSet.getString(ROLE_TYPE)));
        session.setSignature(resultSet.getString(SIGNATURE));
        return session;
    }

    @NotNull
    private PreparedStatement getStatement(@NotNull final Session entity, @NotNull final String query) throws SQLException {
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getUserId());
        statement.setLong(3, entity.getTimestamp());
        statement.setString(4, String.valueOf(entity.getRoleType()));
        statement.setString(5, entity.getSignature());
        return statement;
    }

}
