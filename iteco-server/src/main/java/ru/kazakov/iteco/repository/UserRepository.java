package ru.kazakov.iteco.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.repository.IUserRepository;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.util.DateUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import static ru.kazakov.iteco.constant.Constant.*;
import static ru.kazakov.iteco.util.DateUtil.*;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private final Connection databaseConnection;

    public UserRepository(@NotNull final Connection databaseConnection) {this.databaseConnection = databaseConnection;}

    @Override
    public void persist(@NotNull final User entity) throws Exception {
        @NotNull final String query = "insert into task_manager.user values (?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = getStatement(entity, query);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void merge(@NotNull final User entity) throws Exception {
        @NotNull final String queryExist = String.format("select exists (select %s from task_manager.user where %s = ? )",
                ID, ID);
        @NotNull final PreparedStatement statementExist = databaseConnection.prepareStatement(queryExist);
        statementExist.setString(1, entity.getId());
        @NotNull final ResultSet resultSet = statementExist.executeQuery();
        resultSet.next();
        final boolean exist = resultSet.getBoolean(1);
        resultSet.close();
        statementExist.close();
        if (!exist) {
            persist(entity);
            return;
        }
        @NotNull String query = "update task_manager.user set ";
        if (entity.getDateStart() != null) query = query + DATE_START + " = ?, ";
        if (entity.getDateFinish() != null) query = query + DATE_FINISH + " = ?, ";
        if (entity.getRoleType() != null) query = query + ROLE_TYPE + " = ?, ";
        if (entity.getPassword() != null && !entity.getPassword().isEmpty()) query = query + PASSWORD + " = ?";
        if (entity.getName() != null && !entity.getName().isEmpty()) query = query + NAME + " = ?";
        if (query.endsWith(", ")) query = query.substring(0, query.length() - 2);
        query = query + " where " + ID + " = ? " ;
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        int counter = 1;
        if (entity.getDateStart() != null) {statement.setDate(counter, DateUtil.getSqlDate(entity.getDateStart())); counter++;}
        if (entity.getDateFinish() != null) {statement.setDate(counter, DateUtil.getSqlDate(entity.getDateFinish())); counter++;}
        if (entity.getRoleType() != null) {statement.setString(counter, String.valueOf(entity.getRoleType())); counter++;}
        if (entity.getPassword() != null && !entity.getPassword().isEmpty()) {statement.setString(counter, entity.getPassword()); counter++;}
        if (entity.getName() != null && !entity.getName().isEmpty()) {statement.setString(counter, entity.getName()); counter++;}
        statement.setString(counter, entity.getId());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void remove(@NotNull final String id) throws Exception {
        @NotNull final String query = String.format("delete from task_manager.user where %s = ?", ID);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, id);
        statement.execute();
        statement.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String query = "delete from task_manager.user";
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.execute();
        statement.close();
    }

    @Nullable
    @Override
    public User findOne(@NotNull final String id) throws Exception {
        @NotNull final String query = String.format("select * from task_manager.user where %s = ?", ID);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        @Nullable final User user = fetch(resultSet);
        resultSet.close();
        statement.close();
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) throws SQLException {
        @NotNull final String query = String.format("select * from task_manager.user where %s = ? ", LOGIN);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        @Nullable final User user = fetch(resultSet);
        resultSet.close();
        statement.close();
        return user;
    }

    @NotNull
    @Override
    public List<User> findAll() throws Exception {
        @NotNull final String query = "select * from task_manager.user";
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<User> users = new ArrayList<>();
        while (resultSet.next()) {
            users.add(fetch(resultSet));
        }
        resultSet.close();
        statement.close();
        return users;
    }

    @NotNull
    @Override
    public List<String> findAllUsers() throws SQLException {
        @NotNull final String query = String.format("select %s from task_manager.user", NAME);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<String> names = new ArrayList<>();
        while (resultSet.next()) {
            names.add(resultSet.getString(1));
        }
        resultSet.close();
        statement.close();
        return names;
    }

    @Override
    public boolean contains(@NotNull final String login) throws SQLException {
        @NotNull final String query = String.format("select exists (select %s from task_manager.user where %s = ?)",
                ID, LOGIN);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, login);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        final boolean result = resultSet.getBoolean(1);
        resultSet.close();
        statement.close();
        return result;
    }

    @Nullable
    private User fetch(@Nullable final ResultSet resultSet) throws SQLException {
        if (resultSet == null) return null;
        @NotNull final User user = new User();
        user.setId(resultSet.getString(ID));
        user.setName(resultSet.getString(NAME));
        user.setLogin(resultSet.getString(LOGIN));
        user.setPassword(resultSet.getString(PASSWORD));
        user.setDateCreate(getDateFromSql(resultSet.getDate(DATE_CREATE)));
        user.setDateStart(getDateFromSql(resultSet.getDate(DATE_START)));
        user.setDateFinish(getDateFromSql(resultSet.getDate(DATE_FINISH)));
        user.setRoleType(RoleType.valueOf(resultSet.getString(ROLE_TYPE)));
        return user;
    }

    @NotNull
    private PreparedStatement getStatement(@NotNull final User entity, @NotNull final String query) throws SQLException {
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getName());
        statement.setString(3, entity.getLogin());
        statement.setString(4, entity.getPassword());
        statement.setDate(5, getSqlDate(entity.getDateCreate()));
        statement.setDate(6, getSqlDate(entity.getDateStart()));
        statement.setDate(7, getSqlDate(entity.getDateFinish()));
        statement.setString(8, String.valueOf(entity.getRoleType()));
        return statement;
    }

}
