package ru.kazakov.iteco.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.repository.IProjectRepository;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.enumeration.SortType;
import ru.kazakov.iteco.enumeration.Status;
import ru.kazakov.iteco.util.DateUtil;
import java.sql.*;
import java.util.*;
import static ru.kazakov.iteco.util.DateUtil.*;
import static ru.kazakov.iteco.constant.Constant.*;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @NotNull
    private final Connection databaseConnection;

    public ProjectRepository(@NotNull final Connection databaseConnection) {this.databaseConnection = databaseConnection;}

    @Override
    public void persist(@NotNull final Project entity) throws Exception {
        @NotNull final String query = "insert into task_manager.project values (?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = getStatement(entity, query);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void merge(@NotNull final Project entity) throws Exception {
        @NotNull final String queryExist = String.format("select exists (select %s from task_manager.project where %s = ? )",
                ID, ID);
        @NotNull final PreparedStatement statementExist = databaseConnection.prepareStatement(queryExist);
        statementExist.setString(1, entity.getId());
        @NotNull final ResultSet resultSet = statementExist.executeQuery();
        resultSet.next();
        final boolean exist = resultSet.getBoolean(1);
        resultSet.close();
        statementExist.close();
       if (!exist) {
           persist(entity);
           return;
       }
       @NotNull String query = "update task_manager.project set ";
       if (entity.getDateStart() != null) query = query + DATE_START + " = ?, ";
       if (entity.getDateFinish() != null) query = query + DATE_FINISH + " = ?, ";
       if (entity.getStatus() != null) query = query + STATUS + " = ?, ";
       if (entity.getInformation() != null && !entity.getInformation().isEmpty()) query = query + INFORMATION + " = ?";
       if (query.endsWith(", ")) query = query.substring(0, query.length() - 2);
       query = query + " where " + ID + " = ? " ;
       @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
       int counter = 1;
        if (entity.getDateStart() != null) {statement.setDate(counter, DateUtil.getSqlDate(entity.getDateStart())); counter++;}
        if (entity.getDateFinish() != null) {statement.setDate(counter, DateUtil.getSqlDate(entity.getDateFinish())); counter++;}
        if (entity.getStatus() != null) {statement.setString(counter, String.valueOf(entity.getStatus())); counter++;}
        if (entity.getInformation() != null && !entity.getInformation().isEmpty()) {statement.setString(counter, entity.getInformation()); counter++;}
       statement.setString(counter, entity.getId());
       statement.executeUpdate();
       statement.close();
    }

    @Override
    public void remove(@NotNull final String id) throws Exception {
        @NotNull final String query = String.format("delete from task_manager.project where %s = ?", ID);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, id);
        statement.execute();
        statement.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final String query = "delete from task_manager.project";
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.execute();
        statement.close();
    }

    @Override
    public void removeAll(@NotNull final String currentUserId) throws Exception {
        @NotNull final String query = String.format("delete from task_manager.project where %s = ?", USER_ID);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, currentUserId);
        statement.execute();
        statement.close();
    }

    @Nullable
    @Override
    public Project findOne(@NotNull final String id) throws Exception {
        @NotNull final String query = String.format("select * from task_manager.project where %s = ?", ID);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        @Nullable final Project project = fetch(resultSet);
        resultSet.close();
        statement.close();
        return project;
    }

    @Nullable
    @Override
    public Project findByName(@NotNull final String currentUserId, @NotNull final String name) throws Exception {
        @NotNull final String query = String.format("select * from task_manager.project where %s = ? and %s = ?" , USER_ID,  NAME);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, currentUserId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        @Nullable final Project project = fetch(resultSet);
        resultSet.close();
        statement.close();
        return project;
    }

    @NotNull
    @Override
    public List<Project> findAll() throws Exception {
        @NotNull final String query = "select * from task_manager.project";
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Project> projects = new ArrayList<>();
        while (resultSet.next()) {
            projects.add(fetch(resultSet));
        }
        resultSet.close();
        statement.close();
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String currentUserId) throws Exception {
        @NotNull final String query = String.format("select * from task_manager.project where %s = ? ", ID);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, currentUserId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Project> projects = new ArrayList<>();
        while (resultSet.next()) {
            projects.add(fetch(resultSet));
        }
        resultSet.close();
        statement.close();
        return projects;
    }

    @NotNull
    @Override
    public List<String> findAll(@NotNull final String currentUserId, @NotNull final SortType sortType) throws Exception {
        @NotNull final String orderBy;
        switch (sortType) {
            case START   : orderBy = DATE_START; break;
            case FINISH  : orderBy = DATE_FINISH; break;
            case STATUS  : orderBy = String.format("field(status, '%s','%s','%s')",
                                     String.valueOf(Status.PLANNED),
                                     String.valueOf(Status.IN_PROGRESS),
                                     String.valueOf(Status.READY)); break;
            default      : orderBy = DATE_CREATE;
        }
        @NotNull final String query = String.format("select %s from task_manager.project where %s = ? order by %s",
                NAME, USER_ID, orderBy);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, currentUserId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<String> names = new ArrayList<>();
        while (resultSet.next()) {
            names.add(resultSet.getString(1));
        }
        resultSet.close();
        statement.close();
        return names;
    }

    @NotNull
    @Override
    public List<String> findAllByName(@NotNull final String currentUserId, @NotNull final String part) throws Exception {
        @NotNull final String query = String.format("select %s from task_manager.project where %s = ? and %s like ?" ,
                NAME, USER_ID, NAME);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, currentUserId);
        statement.setString(2, "%" + part + "%");
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<String> names = new ArrayList<>();
        while (resultSet.next()) {
            names.add(resultSet.getString(1));
        }
        resultSet.close();
        statement.close();
        return names;
    }

    @NotNull
    @Override
    public List<String> findAllByInfo(@NotNull final String currentUserId, @NotNull final String part) throws Exception {
        @NotNull final String query = String.format("select %s from task_manager.project where %s = ? and %s like ?" ,
                NAME, USER_ID, INFORMATION);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, currentUserId);
        statement.setString(2, "%" + part + "%");
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<String> names = new ArrayList<>();
        while (resultSet.next()) {
            names.add(resultSet.getString(1));
        }
        resultSet.close();
        statement.close();
        return names;
    }

    @Override
    public boolean contains(@NotNull final String name) throws Exception {
        @NotNull final String query = String.format("select exists (select %s from task_manager.project where %s = ? )",
                ID, NAME);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        final boolean result = resultSet.getBoolean(1);
        resultSet.close();
        statement.close();
        return result;
    }

    @Override
    public boolean contains(@NotNull final String currentUserId, @NotNull final String name) throws Exception {
        @NotNull final String query = String.format("select exists (select %s from task_manager.project where %s = ? and %s = ? )",
                ID, USER_ID, NAME);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, currentUserId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        final boolean result = resultSet.getBoolean(1);
        resultSet.close();
        statement.close();
        return result;
    }

    @Override
    public boolean isEmpty(@NotNull final String id) throws Exception {
        @NotNull final String query = String.format("select isnull(%s) from task_manager.project where %s = ?", INFORMATION, ID);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        final boolean result = resultSet.getBoolean(1);
        resultSet.close();
        statement.close();
        return result;
    }

    @Override
    public boolean isEmptyRepository(@NotNull final String currentUserId) throws Exception {
        @NotNull final String query = String.format("select not exists (select * from task_manager.project where %s = ? )", USER_ID);
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, currentUserId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        final boolean result = resultSet.getBoolean(1);
        resultSet.close();
        statement.close();
        return result;
    }

    @Nullable
    private Project fetch(@Nullable final ResultSet resultSet) throws SQLException {
        if (resultSet == null) return null;
        @NotNull final Project project = new Project();
        project.setId(resultSet.getString(ID));
        project.setUserId(resultSet.getString(USER_ID));
        project.setName(resultSet.getString(NAME));
        project.setDateCreate(getDateFromSql(resultSet.getDate(DATE_CREATE)));
        project.setDateStart(getDateFromSql(resultSet.getDate(DATE_START)));
        project.setDateFinish(getDateFromSql(resultSet.getDate(DATE_FINISH)));
        project.setStatus(Status.valueOf(resultSet.getString(STATUS)));
        project.setInformation(resultSet.getString(INFORMATION));
        return project;
    }

    @NotNull
    private PreparedStatement getStatement(@NotNull final Project entity, @NotNull final String query) throws SQLException {
        @NotNull final PreparedStatement statement = databaseConnection.prepareStatement(query);
        statement.setString(1, entity.getId());
        statement.setString(2, entity.getUserId());
        statement.setString(3, entity.getName());
        statement.setDate(4, getSqlDate(entity.getDateCreate()));
        statement.setDate(5, getSqlDate(entity.getDateStart()));
        statement.setDate(6, getSqlDate(entity.getDateFinish()));
        statement.setString(7, String.valueOf(entity.getStatus()));
        statement.setString(8, entity.getInformation());
        return statement;
    }

}
