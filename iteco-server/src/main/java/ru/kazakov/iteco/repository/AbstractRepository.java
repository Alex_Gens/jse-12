package ru.kazakov.iteco.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.repository.IRepository;
import ru.kazakov.iteco.entity.AbstractEntity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    @Override
    public abstract void persist(@NotNull final T entity) throws Exception;

    @Override
    public abstract void remove(@NotNull final String id) throws Exception;

    @Override
    public abstract void removeAll() throws Exception;

    @Nullable
    @Override
    public abstract T findOne(@NotNull final String id) throws Exception;

    @NotNull
    @Override
    public abstract List<T> findAll() throws Exception;

}
