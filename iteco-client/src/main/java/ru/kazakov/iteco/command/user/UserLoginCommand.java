package ru.kazakov.iteco.command.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.endpoint.ISessionEndpoint;
import ru.kazakov.iteco.api.endpoint.Session;

@NoArgsConstructor
public final class UserLoginCommand extends UserAbstractCommand{

    private final boolean secure = false;

    @Getter
    @NotNull
    private final String name = "user-login";

    @Getter
    @NotNull
    private final String description = "User authorization.";

    @Override
    public boolean isSecure() {return secure;}

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (userEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        terminalService.write("ENTER LOGIN: ");
        @NotNull final String login = terminalService.enterIgnoreEmpty();
        terminalService.write("ENTER PASSWORD: ");
        @NotNull final String password = terminalService.enterIgnoreEmpty();
        if (!userEndpoint.containsUser(login)) {
            terminalService.write("[NOT CORRECT]");
            terminalService.write("User with that login or password doesn't exist.");
            terminalService.separateLines();
            return;
        }
        @NotNull final ISessionEndpoint sessionEndpoint = serviceLocator.getSessionEndpoint();
        @Nullable final Session userSession = sessionEndpoint.getInstanceSession(login, password);
        if (userSession == null) {
            terminalService.write("[NOT CORRECT]");
            terminalService.write("User with that login or password doesn't exist.");
            terminalService.separateLines();
            return;
        }
        terminalService.write("[CORRECT]");
        sessionEndpoint.persistSession(userSession);
        currentState.setUserSession(userSession);
        terminalService.write("Welcome!" + " ["
                + userSession.getRoleType() + "]");
        terminalService.separateLines();
    }

}
