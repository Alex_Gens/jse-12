package ru.kazakov.iteco.command.system;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kazakov.iteco.command.AbstractCommand;

@NoArgsConstructor
public final class AboutCommand extends AbstractCommand {

    private final boolean secure = false;

    @Getter
    @NotNull
    private final String name = "about";

    @Getter
    @NotNull
    private final String description = "Show information about build.";

    @Override
    public boolean isSecure() {return secure;}

    @Override
    public void execute() throws Exception {
        if (terminalService == null) throw new Exception();
        terminalService.write("Build number is: " + Manifests.read("buildNumber"));
        terminalService.write("Developer: " + Manifests.read("developer"));
        terminalService.separateLines();
    }

}
