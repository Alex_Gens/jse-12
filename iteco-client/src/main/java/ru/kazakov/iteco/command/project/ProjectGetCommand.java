package ru.kazakov.iteco.command.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.endpoint.Project;
import ru.kazakov.iteco.api.endpoint.Session;

@NoArgsConstructor
public final class ProjectGetCommand extends ProjectAbstractCommand {

    @Getter
    @NotNull
    private final String name = "project-get";

    @Getter
    @NotNull
    private final String description = "Show all project information.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (projectEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final Session userSession = currentState.getUserSession();
        if (userSession == null) throw new Exception();
        @Nullable final Project project = getProjectByPart();
        if (project == null) return;
        if (project.getInformation() == null || project.getInformation().isEmpty()) {
            terminalService.write("Project is empty. Use \"project-update\" to update this project.");
            terminalService.separateLines();
            return;
        }
        terminalService.write("[Project: " + project.getName() + "]");
        if (project.getInformation() == null) throw new Exception();
        terminalService.write(project.getInformation());
        terminalService.separateLines();
    }

}
