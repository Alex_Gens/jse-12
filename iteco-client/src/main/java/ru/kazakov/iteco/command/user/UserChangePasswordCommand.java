package ru.kazakov.iteco.command.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.endpoint.RoleType;
import ru.kazakov.iteco.api.endpoint.Session;
import ru.kazakov.iteco.api.endpoint.User;
import ru.kazakov.iteco.util.Password;

@NoArgsConstructor
public final class UserChangePasswordCommand extends UserAbstractCommand {

    @Getter
    @NotNull
    private final String name = "user-change-password";

    @Getter
    @NotNull
    private final String description = "Change profile password.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (userEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final Session userSession = currentState.getUserSession();
        if (userSession == null) throw new Exception();
        @Nullable final RoleType userSessionRoleType = userSession.getRoleType();
        if (userSessionRoleType == null) throw new Exception();
        @Nullable User user = null;
        if (userSessionRoleType == RoleType.ADMINISTRATOR) {
            terminalService.write("Enter user's login to change profile password.   [" +
                    userSessionRoleType + "]");
            terminalService.write("ENTER LOGIN: ");
            @NotNull final String login = terminalService.enterIgnoreEmpty();
            boolean isExist = userEndpoint.containsUser(login);
            if (!isExist) {
                terminalService.write("[NOT CORRECT]");
                terminalService.write("User with that login doesn't exist.");
                terminalService.separateLines();
                return;
            }
            user = userEndpoint.findUserByLogin(userSession, login);
            if (user == null) throw new Exception();
        }

        if (userSession.getRoleType() != RoleType.ADMINISTRATOR) {
            user = userEndpoint.findOneUser(userSession, userSession.getUserId());
            checkOldPassword(user);
        }
        @NotNull String firstPassword = "";
        @NotNull String secondPassword = "";
        while (true) {
            terminalService.write("ENTER NEW PASSWORD: ");
            firstPassword = terminalService.enterIgnoreEmpty();
            terminalService.write("Confirm you password.");
            terminalService.write("ENTER NEW PASSWORD: ");
            secondPassword = terminalService.enterIgnoreEmpty();
            if (!firstPassword.equals(secondPassword)) {
                terminalService.write("[NOT UPDATED]");
                terminalService.write("Entered passwords are different.");
                terminalService.separateLines();
                continue;
            }
            terminalService.write("[UPDATED]");
            break;
        }
        @NotNull final String newPassword = Password.getHashedPassword(firstPassword);
        @NotNull final User userToMerge = new User();
        userToMerge.setId(user.getId());
        userToMerge.setDateStart(null);
        userToMerge.setDateFinish(null);
        userToMerge.setLogin(null);
        userToMerge.setPassword(newPassword);
        userToMerge.setName(null);
        userToMerge.setRoleType(null);
        userEndpoint.mergeUser(userSession, userToMerge);
        terminalService.write("Password updated!");
        terminalService.separateLines();
    }

    private void checkOldPassword(@Nullable final User user) throws Exception {
        if (user == null) throw new Exception();
        if (user.getPassword() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        terminalService.write("ENTER PASSWORD: ");
        @NotNull final String entered = terminalService.enterIgnoreEmpty();
        @NotNull final String enteredPassword = Password.getHashedPassword(entered);
        @NotNull final String oldPassword = user.getPassword();
        if (!enteredPassword.equals(oldPassword)) {
            terminalService.write("[NOT CORRECT]");
            terminalService.separateLines();
        }
    }

}
