package ru.kazakov.iteco.command.data.load;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.endpoint.Session;

@NoArgsConstructor
public final class DataLoadJaxbJsonCommand extends DataAbstractLoadCommand {

    @Getter
    @NotNull
    private final String name = "data-load-jaxb-json";

    @Getter
    @NotNull
    private final String description = "Load data in json by JAX-B.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (domainEndpoint == null) throw new Exception();
        @Nullable final Session userSession = currentState.getUserSession();
        if (userSession == null) throw new Exception();
        if (!confirmed()) return;
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final String fileName = "jaxb.json";
        if (!domainEndpoint.isDomainDirectory(userSession, directory)) {
            terminalService.write("[NOT LOADED]");
            terminalService.write("File not exist.");
            terminalService.separateLines();
        }
        if (!domainEndpoint.existDomain(userSession, directory, fileName)) {
            terminalService.write("[NOT LOADED]");
            terminalService.write("File not exist.");
            terminalService.separateLines();
            return;
        }
        domainEndpoint.loadDomainJaxbJson(userSession, directory, fileName);
        currentState.setUserSession(null);
        terminalService.write("[LOADED]");
        terminalService.write("All data successfully loaded!");
        terminalService.separateLines();
    }

}
