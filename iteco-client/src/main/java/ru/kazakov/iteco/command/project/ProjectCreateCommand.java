package ru.kazakov.iteco.command.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.endpoint.Project;
import ru.kazakov.iteco.api.endpoint.Session;

@NoArgsConstructor
public final class ProjectCreateCommand extends ProjectAbstractCommand {

    @Getter
    @NotNull
    private final String name = "project-create";

    @Getter
    @NotNull
    private final String description = "Create new project.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (projectEndpoint == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @Nullable final Session userSession = currentState.getUserSession();
        if (userSession == null) throw new Exception();
        terminalService.write("ENTER PROJECT NAME: ");
        @NotNull final String name = terminalService.enterIgnoreEmpty().trim();
        if (projectEndpoint.containsProjectByCurrentId(userSession, name)) {
            terminalService.write("[NOT CREATED]");
            terminalService.write("Project with this name is already exists. Use another project name.");
            terminalService.separateLines();
            return;
        }
        @NotNull final Project project = new Project();
        project.setUserId(userSession.getUserId());
        project.setName(name);
        projectEndpoint.persistProject(userSession, project);
        terminalService.write("[CREATED]");
        terminalService.write("Project successfully created!");
        terminalService.separateLines();
    }

}
