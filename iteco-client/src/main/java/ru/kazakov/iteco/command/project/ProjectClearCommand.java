package ru.kazakov.iteco.command.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.endpoint.ITaskEndpoint;
import ru.kazakov.iteco.api.endpoint.Session;

@NoArgsConstructor
public final class ProjectClearCommand extends ProjectAbstractCommand {

    @Getter
    @NotNull
    private final String name = "project-clear";

    @Getter
    @NotNull
    private final String description = "Remove all projects.";

    @Override
    public void execute() throws Exception {
        if (serviceLocator == null) throw new Exception();
        if (currentState == null) throw new Exception();
        if (projectEndpoint == null) throw new Exception();
        if (currentState.getUserSession() == null) throw new Exception();
        if (terminalService == null) throw new Exception();
        @NotNull final ITaskEndpoint taskEndpoint = serviceLocator.getTaskEndpoint();
        @Nullable final Session userSession = currentState.getUserSession();
        if (userSession == null) throw new Exception();
        projectEndpoint.removeAllProjectsByCurrentId(userSession);
        taskEndpoint.removeAllTasksWithProjects(userSession);
        terminalService.write("[ALL PROJECTS REMOVED]");
        terminalService.write("Projects successfully removed!");
        terminalService.separateLines();
    }

}
