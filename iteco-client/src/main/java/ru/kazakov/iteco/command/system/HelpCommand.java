package ru.kazakov.iteco.command.system;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.endpoint.RoleType;
import ru.kazakov.iteco.api.endpoint.Session;
import ru.kazakov.iteco.command.AbstractCommand;
import java.util.Comparator;
import java.util.List;

@NoArgsConstructor
public final class HelpCommand extends AbstractCommand {

    private final boolean secure = false;

    @Getter
    @NotNull
    private final String name = "help";

    @Getter
    @NotNull
    private final String description = "Show all commands.";

    @Override
    public boolean isSecure() {return secure;}

    @Override
    public void execute() throws Exception {
        if (terminalService == null) throw new Exception();
        if (currentState == null) throw new Exception();
        @Nullable final Session userSession = currentState.getUserSession();
        @NotNull final List<AbstractCommand> commands = currentState.getCommands();
        commands.sort(Comparator.comparing(AbstractCommand::getName));
        if (userSession != null && userSession.getRoleType() ==  RoleType.ADMINISTRATOR) {
            terminalService.write("[ADMINISTRATOR COMMANDS]");
            commands.forEach(v -> {if (v.isAdmin()) terminalService.write(v.getName() + ": " + v.getDescription());});
            terminalService.separateLines();
        }
        terminalService.write("[DEFAULT COMMANDS]");
        commands.forEach(v -> {if (!v.isAdmin()) terminalService.write(v.getName() + ": " + v.getDescription());});
        terminalService.separateLines();
    }

}
