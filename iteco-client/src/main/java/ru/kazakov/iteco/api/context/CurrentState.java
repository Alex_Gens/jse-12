package ru.kazakov.iteco.api.context;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.endpoint.Session;
import ru.kazakov.iteco.command.AbstractCommand;
import java.util.List;

public interface CurrentState {

    @Nullable
    public Session getUserSession();

    public void setUserSession(@Nullable final Session userSession);

    @NotNull
    public List<AbstractCommand> getCommands();

}
